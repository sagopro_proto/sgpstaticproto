PROTOC_FILE_MAIN = ./sgpstaticproto.proto
PROTOC = protoc
PROTOC_FLAGS = -I . --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative
GIT_ALL = git add . && git commit -m "--" && git push origin master

git_all:
	@echo "git all"
	$(GIT_ALL)

main:
	@echo "create sgp product proto..."
	$(PROTOC) $(PROTOC_FLAGS) $(PROTOC_FILE_MAIN)
	@echo "create sgp product proto..."
	
all: main

.PHONY: main
